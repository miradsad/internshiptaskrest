package com.restapi.mir.controller;

import com.restapi.mir.entity.Album;
import com.restapi.mir.services.AlbumService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@RestController
@RequestMapping("/api/albums")
public class AlbumController {

    private final AlbumService albumService;
    private final Map<Integer, Album> albumCache = new ConcurrentHashMap<>();

    @Autowired
    public AlbumController(AlbumService albumService) {
        this.albumService = albumService;
    }

    @GetMapping
    public Album[] getAllAlbums() {
        log.info("Getting all albums...");
        return albumService.getAllAlbums();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Album> getAlbumById(@PathVariable int id) {
        log.info("Getting album with id {}", id);
        Album cachedAlbum = albumCache.get(id);
        if (cachedAlbum != null) {
            log.info("Album with id {} found in cache", id);
            return ResponseEntity.ok(cachedAlbum);
        } else {
            Album album = albumService.getAlbumById(id);
            if (album != null) {
                albumCache.put(id, album);
                log.info("Album with id {} moved from the service and cached", id);
                return ResponseEntity.ok(album);
            } else {
                log.warn("Album with id {} not found", id);
                return ResponseEntity.notFound().build();
            }
        }
    }

    @PostMapping
    public ResponseEntity<Album> createAlbum(@RequestBody Album album) {
        log.info("Creating album: {}", album);
        Album createdAlbum = albumService.createAlbum(album);
        albumCache.put(createdAlbum.getId(), createdAlbum);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdAlbum);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> updateAlbum(@PathVariable int id, @RequestBody Album album) {
        log.info("Updating album with id {}: {}", id, album);
        albumService.updateAlbum(id, album);
        albumCache.put(id, album);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteAlbum(@PathVariable int id) {
        log.info("Deleting album with id {}", id);
        albumService.deleteAlbum(id);
        albumCache.remove(id);
        return ResponseEntity.noContent().build();
    }
}
