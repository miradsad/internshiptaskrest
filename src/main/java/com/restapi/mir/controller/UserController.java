package com.restapi.mir.controller;

import com.restapi.mir.entity.User;
import com.restapi.mir.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;
    private final Map<Integer, User> userCache = new ConcurrentHashMap<>();

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public ResponseEntity<User[]> getAllUsers() {
        log.info("Getting all users");
        return ResponseEntity.ok(userService.getAllUsers());
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> getUserById(@PathVariable int id) {
        log.info("Getting user with id {}", id);
        User cachedUser = userCache.get(id);
        if (cachedUser != null) {
            log.info("User with id {} found in cache", id);
            return ResponseEntity.ok(cachedUser);
        } else {
            User user = userService.getUserById(id);
            if (user != null) {
                userCache.put(id, user);
                log.info("User with id {} moved from the service and cached", id);
                return ResponseEntity.ok(user);
            } else {
                log.warn("User with id {} not found", id);
                return ResponseEntity.notFound().build();
            }
        }
    }

    @PostMapping
    public ResponseEntity<User> createUser(@RequestBody User user) {
        log.info("Creating user: {}", user);
        User createdUser = userService.createUser(user);
        userCache.put(createdUser.getId(), createdUser);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdUser);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> updateUser(@PathVariable int id, @RequestBody User user) {
        log.info("Updating user with id {}: {}", id, user);
        userService.updateUser(id, user);
        // Обновляем данные в кэше
        userCache.put(id, user);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable int id) {
        log.info("Deleting user with id {}", id);
        userService.deleteUser(id);
        userCache.remove(id);
        return ResponseEntity.noContent().build();
    }
}

