package com.restapi.mir.controller;

import com.restapi.mir.entity.Post;
import com.restapi.mir.services.PostService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@RestController
@RequestMapping("/api/posts")
public class PostController {

    private final PostService postService;
    private final Map<Integer, Post> postCache = new ConcurrentHashMap<>();

    @Autowired
    public PostController(PostService postService) {
        this.postService = postService;
    }

    @GetMapping
    public Post[] getAllPosts() {
        log.info("Getting all posts...");
        return postService.getAllPosts();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Post> getPostById(@PathVariable int id) {
        log.info("Getting post with id {}", id);
        Post cachedPost = postCache.get(id);
        if (cachedPost != null) {
            log.info("Post with id {} found in cache", id);
            return ResponseEntity.ok(cachedPost);
        } else {
            Post post = postService.getPostById(id);
            if (post != null) {
                postCache.put(id, post);
                log.info("Post with id {} moved from the service and cached", id);
                return ResponseEntity.ok(post);
            } else {
                log.warn("Post with id {} not found", id);
                return ResponseEntity.notFound().build();
            }
        }
    }

    @PostMapping
    public ResponseEntity<Post> createPost(@RequestBody Post post) {
        log.info("Creating post: {}", post);
        Post createdPost = postService.createPost(post);
        postCache.put(createdPost.getId(), createdPost);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdPost);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> updatePost(@PathVariable int id, @RequestBody Post post) {
        log.info("Updating post with id {}: {}", id, post);
        postService.updatePost(id, post);
        postCache.put(id, post);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletePost(@PathVariable int id) {
        log.info("Deleting post with id {}", id);
        postService.deletePost(id);
        postCache.remove(id);
        return ResponseEntity.noContent().build();
    }
}
