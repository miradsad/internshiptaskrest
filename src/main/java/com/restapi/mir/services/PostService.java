package com.restapi.mir.services;

import com.restapi.mir.entity.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class PostService {

    private final String BASE_URL = "https://jsonplaceholder.typicode.com";
    private final RestTemplate restTemplate;

    @Autowired
    public PostService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public Post[] getAllPosts() {
        return restTemplate.getForObject(BASE_URL + "/posts", Post[].class);
    }

    public Post getPostById(int id) {
        return restTemplate.getForObject(BASE_URL + "/posts/" + id, Post.class);
    }

    public Post createPost(Post post) {
        return restTemplate.postForObject(BASE_URL + "/posts", post, Post.class);
    }

    public void updatePost(int id, Post post) {
        restTemplate.put(BASE_URL + "/posts/" + id, post);
    }

    public void deletePost(int id) {
        restTemplate.delete(BASE_URL + "/posts/" + id);
    }

}