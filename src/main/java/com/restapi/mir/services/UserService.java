package com.restapi.mir.services;

import com.restapi.mir.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class UserService {

    private final RestTemplate restTemplate;
    private final String BASE_URL = "https://jsonplaceholder.typicode.com";

    @Autowired
    public UserService(RestTemplate restTemplate){
        this.restTemplate = restTemplate;
    }

    public User[] getAllUsers() {
        return restTemplate.getForObject(BASE_URL + "/users", User[].class);
    }

    public User getUserById(int id) {
        return restTemplate.getForObject(BASE_URL + "/users/" + id, User.class);
    }

    public User createUser(User user) {
        return restTemplate.postForObject(BASE_URL + "/users", user, User.class);
    }

    public void updateUser(int id, User user) {
        restTemplate.put(BASE_URL + "/users/" + id, user);
    }

    public void deleteUser(int id) {
        restTemplate.delete(BASE_URL + "/users/" + id);
    }
}
