package com.restapi.mir.services;

import com.restapi.mir.entity.Album;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
@Slf4j
@Service
public class AlbumService {

    private final String BASE_URL = "https://jsonplaceholder.typicode.com";
    private final RestTemplate restTemplate;

    @Autowired
    public AlbumService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public Album[] getAllAlbums() {
        return restTemplate.getForObject(BASE_URL + "/albums", Album[].class);
    }

    public Album getAlbumById(int id) {
        return restTemplate.getForObject(BASE_URL + "/albums/" + id, Album.class);
    }

    public Album createAlbum(Album album) {
        return restTemplate.postForObject(BASE_URL + "/albums", album, Album.class);
    }

    public void updateAlbum(int id, Album album) {
        restTemplate.put(BASE_URL + "/albums/" + id, album);
    }

    public void deleteAlbum(int id) {
        restTemplate.delete(BASE_URL + "/albums/" + id);
    }

}